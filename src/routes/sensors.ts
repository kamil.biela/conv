import * as express from 'express';
import * as yup from 'yup';
import * as sensorPropsValidator from '../validations/sensorPropsValidations';
import { SensorEntry } from '../model/SensorEntry';
import { SensorService } from '../service/SensorService';
import { SensorsMonitor } from '../service/sensorAlerts/SensorsMonitor';

export function putSensorData(
  sensorService: SensorService,
  sensorsMonitor: SensorsMonitor,
): express.RequestHandler {
  const validateBodySchema = yup.object().shape({
    sensorId: sensorPropsValidator.sensorIdValidtor.required(),
    time: sensorPropsValidator.time.required(),
    value: sensorPropsValidator.value.required(),
  });

  return (req, res, next) => (async (): Promise<any> => {
    const incomingData = req.body;
    try {
      await validateBodySchema.validate(incomingData);
    } catch (e) {
      return res
        .status(400)
        .json({ error: e.message });
    }

    const data: SensorEntry = validateBodySchema.cast(incomingData);

    try {
      await sensorService.persist(data);
    } catch (e) {
      if (e.isDuplicate) {
        return res.status(409).json({ error: 'duplicate' });
      }

      console.error(e); // @todo something better + log request c

      return res
        .status(500)
        .json({ error: 'server error' });
    }

    res.status(204).send();

    setImmediate(() => {
      sensorsMonitor.notifyObservers(data);
    });

  })().catch(next);
}

export function getSensorData(
  sensorService: SensorService,
): express.RequestHandler {
  const validateQuerySchema = yup.object().shape({
    sensorId: sensorPropsValidator.sensorIdValidtor.required(),
    since: sensorPropsValidator.time,
    until: sensorPropsValidator.time,
    next: yup.number().positive().optional().default(0),
  });

  return (req, res, next) => {
    (async (): Promise<any> => {
      const queryParams = validateQuerySchema.cast(req.query);
      const data = await sensorService.find({...queryParams});
      return res.json(data);
    })().catch(next);
  }
}
