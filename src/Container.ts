import { SensorService } from "./service/SensorService";
import { SensorsMonitor } from "./service/sensorAlerts/SensorsMonitor";
import { Config } from "./model/Config";

export interface Container {
  config: Config;
  sensorService: SensorService;
  sensorMonitor: SensorsMonitor;
}
