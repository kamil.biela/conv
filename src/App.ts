import express from 'express';
import * as routes from './routes';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import { Container } from './Container';

export class App {
  private app: express.Express;
  private server: http.Server;

  constructor(
    private container: Container,
  ) {}

  private configureRoutes(): void {
    if (!this.app) {
      throw new Error('this.app not initialized');
    }

    this.app.get('/data', routes.getSensorData(
      this.container.sensorService,
    ));
    this.app.put('/data', routes.putSensorData(
      this.container.sensorService,
      this.container.sensorMonitor,
    ));
  }

  private async initHttp(): Promise<void> {
    if (this.server) {
      throw new Error('Http server already initialized')
    }

    return new Promise(resolve => {
      this.app = express();
      this.app.use(bodyParser.json());

      this.configureRoutes();

      this.server = this.app.listen(this.container.config.httpPort, resolve);
    });
  }

  async stop(): Promise<void> {
    return new Promise(resolve => {
      if (!this.app) {
        return;
      }
      this.server.once('close', () => {
        resolve();
      });
      this.server.close();
    });
  }

  async start(): Promise<void> {
    await this.initHttp();
    console.log(`Listening on ${this.container.config.httpPort}`)
  }
}
