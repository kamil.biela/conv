import { buildApp } from './buildApp';
import * as process from 'process';

const app = buildApp({
  httpPort: parseInt(process.env.HTTP_PORT) || 8080,
  dbUrl: process.env.DB_URL || 'postgres://postgres:pass@localhost:5432/postgres',
});

app.start().catch(x => console.error(x));
