export interface SensorEntry {
  sensorId: string;
  time: number;
  value: number;
}
