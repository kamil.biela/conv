export interface Config {
  httpPort: number;
  dbUrl: string;
}
