import { SensorEntry } from "../model/SensorEntry";
import Knex from "knex";
import * as _ from 'lodash';

interface FindParams {
  sensorId: string;
  since?: number;
  until?: number;
  next: number;
}

interface FindModel {
  data: SensorEntry[],

  meta: {
    next: number;
  }
}

export class SensorService {
  constructor(
    private knex: Knex
  ) {
  }

  async persist(data: SensorEntry): Promise<void> {
    try {
      return await this.knex('sensor_entries')
        .insert(data);
    } catch (e) {
      if (e.code === '23505') {
        e.isDuplicate = true;
      }

      throw e;
    }
  }

  async find(params: FindParams): Promise<FindModel> {
    const limit = 100;

    const select = this.knex<SensorEntry>('sensor_entries')
      .select();

    if (!_.isNil(params.sensorId)) {
      select.where('sensorId', params.sensorId);
    }

    if (!_.isNil(params.since)) {
      select.where('time', '>', params.since);
    }

    if (!_.isNil(params.until)) {
      select.where('time', '<=', params.until);
    }

    const data = await select.limit(limit)
      .offset(limit * (params.next || 0));

    return {
      data,
      meta: {
        next: (data.length === limit) ? params.next + 1 : undefined,
      }
    };
  }
}
