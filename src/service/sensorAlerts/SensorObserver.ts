import { SensorEntry } from "../../model/SensorEntry";

export interface SensorObserver {
  update(data: SensorEntry): any;
}
