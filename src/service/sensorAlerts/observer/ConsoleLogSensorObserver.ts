import { SensorObserver } from "../SensorObserver";
import { SensorEntry } from "../../../model/SensorEntry";

export class ConsoleLogSensorObserver implements SensorObserver {
  update(data: SensorEntry) {
    if (data.value > 80) {
      console.log(`WARN: sensor ${data.sensorId} value of ${data.value} crossed treshold of 80`);
    } else if (data.value > 0) {
      console.log(`INFO: sensor ${data.sensorId} value of ${data.value} crossed treshold of 0`);
    }
  }
}
