import { SensorObserver } from "./SensorObserver";
import { SensorEntry } from "../../model/SensorEntry";

export class SensorsMonitor {
  private observers: SensorObserver[] = [];

  addObserver(observer: SensorObserver) {
    this.observers.push(observer);
  }

  notifyObservers(data: SensorEntry) {
    this.observers.forEach(x => x.update(data));
  }
}
