import * as yup from 'yup';

export const sensorIdValidtor = yup.string();
export const time = yup.number().positive().integer();
export const value = yup.number();
