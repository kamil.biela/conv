import { App } from "./App"
import { Config } from "./model/Config";
import { Container } from "./Container";
import { SensorService } from "./service/SensorService";
import { SensorsMonitor } from "./service/sensorAlerts/SensorsMonitor";
import { ConsoleLogSensorObserver } from "./service/sensorAlerts/observer/ConsoleLogSensorObserver";
import camelcaseKeys from 'camelcase-keys';
import * as knex from 'knex';
import * as _ from 'lodash';

export const buildApp = (config: Config) => {
  const sensorMonitor = new SensorsMonitor();
  // if needed, add more observers that notify by email, sms, etc.
  // @todo it might make sense to add `sensorId` paramter to `addObserver` method
  // so the observer is fired only for given sensor. Or that addObserver accepts sensor type
  // and SensorObserver is added per sensor type. It depends on how many
  // sensors are there and how many types. Current implementation is good base to
  // go in each of these directions, or all
  sensorMonitor.addObserver(
    new ConsoleLogSensorObserver()
  );

  const knexInstance = knex.default({
    client: 'pg',
    connection: config.dbUrl,
    postProcessResponse: (result, queryContext) => {
      return camelcaseKeys(result);
    },
    wrapIdentifier: (value, origImpl, queryContext) => origImpl(_.snakeCase(value)),
    pool: { min: 0, max: 1 },
    asyncStackTraces: true, // @todo make configurable
  })

  const sensorService = new SensorService(knexInstance);

  const container: Container = {
    config,
    sensorMonitor,
    sensorService,
  }

  return new App(container);
}
