module.exports = {
  automock: false,
  preset: 'ts-jest',
  testEnvironment: 'node'
}
