CREATE TABLE sensor_entries (
  sensor_id VARCHAR(255) NOT NULL,
  time      INT          NOT NULL,
  value     FLOAT        NOT NULL
);

SELECT create_hypertable('sensor_entries', 'time', chunk_time_interval => 86400000);

CREATE UNIQUE INDEX ON sensor_entries (sensor_id, time DESC);
