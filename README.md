Things to fix:
 - add tests
 - store and parse sensor data value as something else than float if value precision matters

To run:
```sh
npm run start:dev
npm run setup
npm install
npm start
```
